@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">User Authenticated</div>

                    <div class="flex-center position-ref full-height">
                        <div class="content">
                            <div class="links">
                                <a href="{{ route('authapitest') }}">Test Authorized WSO2 API</a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection

