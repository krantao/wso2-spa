<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WSO2Controller extends Controller
{
    //

    public function wso2Api()
    {
        $testData = array("Name"=>"Kgomotso", "Surname"=>"Rantao", "Location"=>"Dearx", "Status"=>"No Authentication");

        return response()->json($testData, 200);
    }
}
