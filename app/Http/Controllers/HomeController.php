<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function authAPI()
    {
        $User =  User::find(Auth::id());

        $testData = array("Name"=>$User->name, "Surname"=>"", "Email"=>$User->email, "Location"=>"Dearx", "Status"=>"Authenticated");

        return response()->json($testData, 200);
    }
}
